﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Numerics;

namespace palindromeproduct
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();
            //var max = SimplePrimeFactorOptimized(8);
            var max = ComplexModInverseOptimized(12);
            sw.Stop();
            Console.WriteLine(max);
            Console.WriteLine($"Elapsed: {sw.Elapsed.TotalMilliseconds:N2} ms");
        }

        private static bool IsPalindrome(long n, NumberFormatInfo formatInfo)
        {
            return IsPalindrome(n.ToString(formatInfo));
        }
        
        private static bool IsPalindrome(BigInteger n, NumberFormatInfo formatInfo)
        {
            return IsPalindrome(n.ToString(formatInfo));
        }
        
        private static bool IsPalindrome(string s)
        {
            for (var i = 0; i <= s.Length - 1 - i; i++)
            {
                if (s[i] != s[s.Length - 1 - i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Optimized by exploiting the fact that one of the numbers must be divisible by 11. 
        /// </summary>
        /// <remarks>
        /// New best:
        /// 7 digits, 99956644665999, Elapsed: 4.305,98 ms
        /// 8 digits, 9999000000009999, Elapsed: 6.643,12 ms
        ///
        /// Credit: Begoner
        /// 
        /// The palindrome can be written as:
        /// abccba
        ///
        /// Which then simpifies to:
        /// 100000a + 10000b + 1000c + 100c + 10b + a
        ///
        /// And then:
        /// 100001a + 10010b + 1100c
        ///
        /// Factoring out 11, you get:
        /// 11(9091a + 910b + 100c)
        ///
        /// So the palindrome must be divisible by 11. Seeing as 11 is prime, 
        /// at least one of the numbers must be divisible by 11.
        /// </remarks>>
        private static long SimplePrimeFactorOptimized(int digits)
        {
            var max = 0L;
            var formatProvider = CultureInfo.CurrentCulture.NumberFormat;
            var upperBound = (long)Math.Pow(10, digits) - 1;
            var lowerBound = (long)Math.Pow(10, digits-1) - 1;
            var innerUpperBound = upperBound - upperBound % 11;
            for (var i = upperBound; i > lowerBound; i--)
            {
                for (var j = innerUpperBound; j > lowerBound; j -= 11)
                {
                    var n = i * j;
                    if (n <= max)
                        break;
                    if (IsPalindrome(n, formatProvider))
                    {
                        max = n;
                        break;
                    }
                }
            }
            return max;
        }

        
        /// <summary>
        /// Exploits that p = x * y + 1 is divisible by 10^(digits/2) 
        /// </summary>
        /// <remarks>
        /// 8 digits (using long), 9999000000009999, Elapsed: 1,15 ms
        /// 8 digits (using BigInteger), 9999000000009999, Elapsed: 18,36 ms
        /// 9 digits (using BigInteger), 999900665566009999, Elapsed: 90,28 ms
        /// 10 digits (using BigInteger), 99999834000043899999, Elapsed: 27,68 ms
        /// 11 digits (using BigInteger), 9999994020000204999999, Elapsed: 36,33 ms
        /// 12 digits (using BigInteger), 999999000000000000999999, Elapsed: 269,91 ms
        /// 13 digits (using BigInteger), 99999963342000024336999999, Elapsed: 4.357,23 ms
        /// 14 digits (using BigInteger), 9999999000000000000009999999, Elapsed: 2.780,54 ms
        /// 15 digits (using BigInteger), 999999974180040040081479999999, Elapsed: 10.444,53 ms
        /// 16 digits (using BigInteger), 99999999000000000000000099999999, Elapsed: 30.543,58 ms
        /// 17 ????
        /// 18 digits (using BigInteger), 999999999470552640046255074999999999, Elapsed: 184.238,11 ms
        /// Credit: Lucy_Hedgehog
        /// 
        /// This is based on two key insights:
        /// 1. The largest palindrome for n digits starts and ends with floor(n/2) 9's.
        /// 2. Given 1. and a palindrome p = x * y, then p + 1 = x * y + 1 is divisible by 10^(floor(n/2)).
        /// 
        /// This enables us to construct numbers that are divisible in that way and then check if they
        /// are a palindrome. To construct such numbers we use the modular inverse.
        /// See https://en.wikipedia.org/wiki/Modular_multiplicative_inverse.
        /// </remarks>
        private static BigInteger ComplexModInverseOptimized(int digits)
        {
            var formatProvider = CultureInfo.CurrentCulture.NumberFormat;
            var k = digits / 2;
            while (true)
            {
                var done = false;
                var upperBound = (BigInteger)Math.Pow(10, digits) - 1;
                var upperBound_11 = (upperBound - 11) / 22 * 22 + 11;
                var lowerBound = upperBound + 1 - (BigInteger)Math.Pow(10, digits-k) + 1;
                BigInteger max;
                if (2 * k == digits)
                {
                    max = upperBound * lowerBound;
                    done = true;
                }
                else
                    max = lowerBound * lowerBound;
                var mod = (int) Math.Pow(10, k);
            
                for (var i = upperBound_11; i > 1; i -= 11)
                {
                    if (i * upperBound < max)
                        break;
                    var inverse = ModInverse(i, mod);
                    var innerUpperBound = upperBound + 1 - inverse;
                    for (var p = innerUpperBound * i; p > max; p -= i * mod)
                    {
                        if (IsPalindrome(p, formatProvider) && p > max)
                        {
                            done = true;
                            max = p;
                        }
                    }
                }
                
                if (done)
                    return max;
                else
                    k--;
            }
        }
        
        
        /// <summary>
        /// Calculates the mod inverse by performing magic.
        /// </summary>
        /// <remarks>
        /// https://rosettacode.org/wiki/Modular_inverse
        /// </remarks>
        private static BigInteger ModInverse(BigInteger num, BigInteger mod)
        {
            BigInteger i = mod, v = 0, d = 1;
            
            while (num > 0)
            {
                BigInteger t = i / num, x = num;
                num = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            
            v %= mod;
            
            if (v < 0) 
                v = (v + mod) % mod;
            
            return v;
        }
    }
}
